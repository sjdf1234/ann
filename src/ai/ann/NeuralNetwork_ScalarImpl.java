/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann;

import ai.ann.util.MatrixOld;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ai.ann.train.optimisation.IOptimisation;

/**
 *
 * @author samuel
 */
public class NeuralNetwork_ScalarImpl {

    private final int inputSize; // # of input neurons
    private final int outputSize; // # of output neurons
    private final int layerCount; // # of Layers

    private final NeuralLayer_ScalarImpl[] neuralLayers;

    public NeuralNetwork_ScalarImpl(int inputSize, int outputSize, int... hiddenLayerSizes) {
        this.inputSize = inputSize;
        this.outputSize = outputSize;

        int hiddenCount = hiddenLayerSizes.length;
        layerCount = hiddenCount + 1;

        neuralLayers = new NeuralLayer_ScalarImpl[layerCount];

        // Create hidden layers
        for (int i = 0; i < hiddenCount; i++) {
            NeuralLayer_ScalarImpl hidden;
            int layerSize = hiddenLayerSizes[i];

            if (i == 0) {
                hidden = new NeuralLayer_ScalarImpl(layerSize, inputSize);
            } else {
                hidden = new NeuralLayer_ScalarImpl(layerSize, neuralLayers[i - 1].getNeuronCount());
            }

            neuralLayers[i] = hidden;
        }

        // Create output layer
        int outputLayerInputCount = hiddenCount > 0 ? neuralLayers[hiddenCount - 1].getNeuronCount() : inputSize;
        NeuralLayer_ScalarImpl outputLayer = new NeuralLayer_ScalarImpl(outputSize, outputLayerInputCount);
        neuralLayers[hiddenCount] = outputLayer;
    }

    public static NeuralNetwork_ScalarImpl load(String filePath) {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            } 
            String networkString = sb.toString();
            
            NeuralNetwork_ScalarImpl network = new Gson().fromJson(networkString, NeuralNetwork_ScalarImpl.class);
            network.init();
            return network;
        } catch (IOException e) {
            System.out.println("Load failed: " + e.getClass().getSimpleName());
            return null;
        } catch (JsonSyntaxException e) {
            System.out.println("Load failed: " + e.getClass().getSimpleName());
            return null;
        }
    }   
    
    public void save(String filePath) {
        File file = new File("assets/net.txt");
        try (FileOutputStream out = new FileOutputStream(file)) {
            String netString = new Gson().toJson(this);
            out.write(netString.getBytes());
        } catch (Exception e) {
            System.out.println("Save failed");
        }
    }

    public void init() {
        for (NeuralLayer_ScalarImpl layer : neuralLayers) {
            layer.init();
        }
    }

    public MatrixOld feedForward(MatrixOld inputs) {
        MatrixOld outputs = inputs;

        for (NeuralLayer_ScalarImpl nl : neuralLayers) {
            outputs = nl.feedForward(outputs);
        }

        return outputs;
    }

    public MatrixOld feedForward(double[] inputArray) {
        MatrixOld inputs = new MatrixOld(inputArray.length, 1);
        inputs.setElements(inputArray);
        return feedForward(inputs);
    }

    public void updateGradients() {
        for (NeuralLayer_ScalarImpl neuralLayer : neuralLayers) {
            neuralLayer.updateGradients();
        }
    }

    /**
     * Updates the weights for each layer using error values This is run after
     * the back prop algorithm has generated these error values
     *
     * @param learningRate
     */
    public void updateWeights(IOptimisation optimisationType, int batchSize, double learningRate) {
        for (NeuralLayer_ScalarImpl neuralLayer : neuralLayers) {
            neuralLayer.updateWeights(optimisationType, batchSize, learningRate);
        }
    }

    public void printWeights() {
        for (NeuralLayer_ScalarImpl neuralLayer : neuralLayers) {
            neuralLayer.getWeights().print();
        }
    }

    public void print() {
        System.out.println("NEURAL NETWORK");
        //System.out.println("Architecture: ");
        //System.out.println("Hyperparameters: ");
        for (int i = 0; i < layerCount; i++) {
            neuralLayers[i].print();
        }
    }

    public NeuralLayer_ScalarImpl[] getNeuralLayers() {
        return neuralLayers;
    }

    public int getInputSize() {
        return inputSize;
    }

    public int getOutputSize() {
        return outputSize;
    }

    public int getLayerCount() {
        return layerCount;
    }

}
