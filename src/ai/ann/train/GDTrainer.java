/*
 * To change this license header, choose License Headers I Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template I the editor.
 */
package ai.ann.train;

import ai.ann.NeuralLayer;
import ai.ann.NeuralNetwork;
import ai.ann.functions.IActivationFunction;
import ai.ann.train.optimisation.IOptimisation;
import ai.ann.train.optimisation.Momentum;
import ai.ann.train.optimisation.Default;
import ai.ann.util.DatasetUtil;
import ai.ann.util.IDataset;
import java.util.Arrays;
import org.jblas.DoubleMatrix;

/**
 *
 * @author samuel
 */
public class GDTrainer {

    private NeuralNetwork network;
    private int size;
    private DoubleMatrix[] weightMatrices;
    private IActivationFunction[] functions;

    private IDataset dataset;
    
    private IOptimisation optimisation;

    public GDTrainer() {
    }
    
    public GDTrainer(NeuralNetwork network, IDataset dataset) {
        setNetwork(network);
        setDataset(dataset);
    }
    
    /**
     * Sets the network to train
     *
     * @param network
     */
    public void setNetwork(NeuralNetwork network) {
        this.network = network;

        // Unpack the network
        size = network.getSize();
        weightMatrices = network.getWeightMatrices(); // Weight Matrices
        functions = network.getFunctions(); // Functions
    }

    /**
     * Sets the dataset to use in training
     *
     * @param dataset
     */
    public void setDataset(IDataset dataset) {
        this.dataset = dataset;
    }

    public void setOptimisation(IOptimisation optimisation) {
        this.optimisation = optimisation;
    }
    
    public void trainStochasticGD(int maxEpochs) {
        assertValidGD(maxEpochs, 1);
        
        epoch:
        for (int epochIndex = 0; epochIndex < maxEpochs; epochIndex++) {
            int[] samples = DatasetUtil.getIndices(dataset);
            DatasetUtil.shuffleIndices(samples);

            sample:
            for (int sampleIndex = 0; sampleIndex < samples.length; sampleIndex++) {
                System.out.print(String.format("[SGD] Epoch[%d/%d] Sample[%d/%d]\r", epochIndex + 1, maxEpochs, sampleIndex + 1, samples.length));
                DoubleMatrix inputVector = dataset.getInput(samples[sampleIndex]);
                DoubleMatrix outputVector = dataset.getOutput(samples[sampleIndex]);

                DoubleMatrix[] activationVectors = calculateActivationVectors(inputVector);
                DoubleMatrix[] errorVectors = calculateErrorVectors(activationVectors, outputVector);
                DoubleMatrix[] gradientMatrices = calculateGradientMatrices(inputVector, activationVectors, errorVectors);
                DoubleMatrix[] deltaWeightMatrices = optimisation.calculateDeltaWeightMatrices(weightMatrices, gradientMatrices);
                updateWeightMatrices(deltaWeightMatrices);
            }
        }
        
        System.out.println("Training Complete");
    }

    public void trainMiniBatchGD(int maxEpochs, int batchSize) {
        assertValidGD(maxEpochs, batchSize);
        
        DoubleMatrix[] meanGradientMatrices = new DoubleMatrix[size];
        for (int i = 0; i < size; i++) {
            meanGradientMatrices[i] = weightMatrices[i].dup();
        }

        epoch:
        for (int epochIndex = 0; epochIndex < maxEpochs; epochIndex++) {
            int[] indices = DatasetUtil.getIndices(dataset);
            DatasetUtil.shuffleIndices(indices);
            int[][] batches = DatasetUtil.getBatches(indices, batchSize);

            batch:
            for (int batchIndex = 0; batchIndex < batches.length; batchIndex++) {
                int[] batch = batches[batchIndex];

                // Zero the batch total
                for (int i = 0; i < size; i++) {
                    meanGradientMatrices[i].fill(0);
                }

                sample:
                for (int sampleIndex = 0; sampleIndex < batch.length; sampleIndex++) {
                    System.out.print(String.format("[Mini-Batch GD] Epoch[%d/%d] Batch[%d/%d] Sample[%d/%d]\r", epochIndex + 1, maxEpochs, batchIndex + 1, batches.length, sampleIndex + 1, batch.length));
                    
                    DoubleMatrix inputVector = dataset.getInput(batch[sampleIndex]);
                    DoubleMatrix outputVector = dataset.getOutput(batch[sampleIndex]);

                    DoubleMatrix[] activationVectors = calculateActivationVectors(inputVector);
                    DoubleMatrix[] errorVectors = calculateErrorVectors(activationVectors, outputVector);
                    DoubleMatrix[] gradientMatrices = calculateGradientMatrices(inputVector, activationVectors, errorVectors);

                    // Add the gradient matrices for this sample to the batch total
                    for (int i = 0; i < size; i++) {
                        meanGradientMatrices[i].addi(gradientMatrices[i]);
                    }
                }

                // Divide the total by batch size to get batch mean
                for (int i = 0; i < size; i++) {
                    meanGradientMatrices[i].divi(batch.length);
                }

                DoubleMatrix[] deltaWeightMatrices
                        = optimisation.calculateDeltaWeightMatrices(weightMatrices, meanGradientMatrices);
                updateWeightMatrices(deltaWeightMatrices);
            }
        }
        
        System.out.println(" Done");
    }

    public void trainBatchGD(int maxEpochs) {
        assertValidGD(maxEpochs, dataset.getSize());
        
        DoubleMatrix[] meanGradientMatrices = new DoubleMatrix[size];
        for (int i = 0; i < size; i++) {
            meanGradientMatrices[i] = weightMatrices[i].dup();
        }

        epoch:
        for (int epochIndex = 0; epochIndex < maxEpochs; epochIndex++) {
            int[] samples = DatasetUtil.getIndices(dataset);

            // Zero the batch total
            for (int i = 0; i < size; i++) {
                meanGradientMatrices[i].fill(0);
            }

            sample:
            for (int sampleIndex = 0; sampleIndex < samples.length; sampleIndex++) {
                System.out.print(String.format("[Batch GD] Epoch[%d/%d] Sample[%d/%d]\r", epochIndex + 1, maxEpochs, sampleIndex + 1, samples.length));
                
                DoubleMatrix inputVector = dataset.getInput(samples[sampleIndex]);
                DoubleMatrix outputVector = dataset.getOutput(samples[sampleIndex]);

                DoubleMatrix[] activationVectors = calculateActivationVectors(inputVector);
                DoubleMatrix[] errorVectors = calculateErrorVectors(activationVectors, outputVector);
                DoubleMatrix[] gradientMatrices = calculateGradientMatrices(inputVector, activationVectors, errorVectors);

                // Add the gradient matrices for this sample to the batch total
                for (int i = 0; i < size; i++) {
                    meanGradientMatrices[i].addi(gradientMatrices[i]);
                }
            }

            // Divide the total by batch size to get batch mean
            for (int i = 0; i < size; i++) {
                meanGradientMatrices[i].divi(samples.length);
            }

            DoubleMatrix[] deltaWeightMatrices
                    = optimisation.calculateDeltaWeightMatrices(weightMatrices, meanGradientMatrices);
            updateWeightMatrices(deltaWeightMatrices);
        }
        
        System.out.println(" Done");
    }

    /**
     * Process input through network, storing intermediates a = f(w * x) for
     * each layer
     *
     * @param index the input to use
     */
    private DoubleMatrix[] calculateActivationVectors(DoubleMatrix inputVector) {
        DoubleMatrix currentInputVector = inputVector;
        DoubleMatrix[] activationVectors = new DoubleMatrix[size];
        // Traverse network in forward direction
        for (int i = 0; i < size; i++) {
            DoubleMatrix activationVector
                    = functions[i].compute(weightMatrices[i].mmul(currentInputVector));
            activationVectors[i] = activationVector; // Store activation
            // Let the input for the next layer be the output of this one
            currentInputVector = activationVector;
        }
        return activationVectors;
    }

    /**
     * Calculate the g of the cost function w.r.t each summed input (dC_dz)
     * dC_dz = dC_da * da_dz
     *
     * @param index the output to use
     * @return
     */
    private DoubleMatrix[] calculateErrorVectors(DoubleMatrix[] activationVectors, DoubleMatrix outputVector) {
        DoubleMatrix[] errorVectors = new DoubleMatrix[size]; // dC_dz
        int i = size - 1; // Move backwards through network
        
        errorVectors[i] = activationVectors[i].sub(outputVector) // dC_da
                .mul(functions[i].computeDerivative(activationVectors[i])); // da_dz 
        for (--i; i >= 0; i--) {
            errorVectors[i] = weightMatrices[i + 1].transpose().mmul(errorVectors[i + 1]) // dC_da
                    .mul(functions[i].computeDerivative(activationVectors[i])); // da_dz
        }
        return errorVectors;
    }

    /**
     * Calculate the g of the cost function w.r.t each weight (dC_dw) dC_dw =
     * dC_dz * dz_dw
     *
     */
    private DoubleMatrix[] calculateGradientMatrices(DoubleMatrix inputVector, DoubleMatrix[] activationVectors, DoubleMatrix[] errorVectors) {
        DoubleMatrix[] gradientMatrices = new DoubleMatrix[size]; // dC_dw
        int i = 0;
        gradientMatrices[i] = errorVectors[i].mmul(inputVector.transpose()); // First layer
        for (++i; i < size; i++) {
            gradientMatrices[i] = errorVectors[i] // dC_dz
                    .mmul(activationVectors[i - 1].transpose()); // dz_dw
        }
        return gradientMatrices;
    }

    /**
     * Set each weightMatrix to its old value plus its respective
     * deltaWeightMatrix
     *
     * @param deltaWeightMatrices
     */
    private void updateWeightMatrices(DoubleMatrix[] deltaWeightMatrices) {
        for (int i = 0; i < size; i++) {
            weightMatrices[i].addi(deltaWeightMatrices[i]);
        }
    }

    private void assertValidGD(int maxEpochs, int batchSize) {
        if (network == null) {
            throw new NullPointerException("network not set");
        }

        if (dataset == null) {
            throw new NullPointerException("dataset not set");
        }
        
        if (optimisation == null) {
            throw new NullPointerException("optimisation not set");
        }

        if (maxEpochs < 1) {
            throw new IllegalArgumentException("maxEpochs must be >= 1");
        }
        
        if (batchSize < 1 || batchSize > dataset.getSize()) {
            throw new IllegalArgumentException("batchSize must be >= 1 and <= dataset.getSize()");
        }
    }
    
}
