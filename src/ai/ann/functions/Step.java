/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.functions;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

/**
 *
 * @author samuel
 */
public class Step implements IActivationFunction {

    @Override
    public DoubleMatrix compute(DoubleMatrix X) {
        return MatrixFunctions.ceil(X);
    }

    @Override
    public DoubleMatrix computeDerivative(DoubleMatrix fX) {
        return fX.dup().fill(0);
    }

    @Override
    public String toString() {
        return "ceil(x)";
    }

}
