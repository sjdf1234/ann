/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.emnist;

import ai.ann.util.ConsoleColours;
import ai.ann.util.ZipLoader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jblas.DoubleMatrix;
import ai.ann.util.IDataset;
import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 *
 * @author samuel
 */
public class EMNISTDataset implements IDataset {

    private final Map<Integer, Character> mapping;
    private final static int IMAGE_WIDTH = 28, IMAGE_HEIGHT = 28, IMAGE_SIZE = IMAGE_WIDTH * IMAGE_HEIGHT;
    public final int LABEL_SIZE;
    private byte[] images;
    private byte[] labels;

    public EMNISTDataset(String path, EMNISTSplit split, EMNISTSet set) {
        ZipLoader loader = new ZipLoader(path);

        String base = "gzip/" + split.string;

        mapping = new HashMap();
        String[] lines = loader.loadTextEntry(base + "-mapping.txt");
        for (String line : lines) {
            String[] values = line.split(" ");
            int i = Integer.parseInt(values[0]);
            char c = (char) Integer.parseInt(values[1]);
            mapping.put(i, c);
        }
        LABEL_SIZE = mapping.size();

        base += set.string;
        images = loader.loadGZIPEntry(base + "-images-idx3-ubyte.gz");
        labels = loader.loadGZIPEntry(base + "-labels-idx1-ubyte.gz");
        
        images = Arrays.copyOfRange(images, 16, images.length);
        labels = Arrays.copyOfRange(labels, 8, labels.length);
    }

    @Override
    public int getSize() {
        return labels.length;
    }

    @Override
    public DoubleMatrix getInput(int i) {
        i *= IMAGE_SIZE;
        double[] image = new double[IMAGE_SIZE];
        for (int p = 0; p < IMAGE_SIZE; p++) {
            image[p] = Byte.toUnsignedInt(images[i + p]) / 255.0;
        }
        return new DoubleMatrix(image);
    }

    @Override
    public DoubleMatrix getOutput(int i) {
        double[] label = new double[LABEL_SIZE];
        label[Byte.toUnsignedInt(labels[i])] = 1;
        return new DoubleMatrix(label);
    }
    
    
    public static DoubleMatrix preprocessImage(BufferedImage image) {
        DoubleMatrix input = new DoubleMatrix(784);
        
        for (int y = 0; y < IMAGE_HEIGHT; y++) {
            for (int x = 0; x < IMAGE_WIDTH; x++) {
                Color color = new Color(image.getRGB(x, y));
                double value = 0.2125 * color.getRed() / 255.0 + 0.7154 * color.getBlue() / 255.0 + 0.0721 * color.getGreen() / 255.0;
                input.put(x * IMAGE_WIDTH + y, value);
            }
        }
        
        return input;
    }
    
    public void printInput(int i) {
        printInput(getInput(i));
    }

    public static void printInput(DoubleMatrix input) {
        double[] image = input.toArray();
        for (int y = 0; y < IMAGE_HEIGHT; y++) {
            for (int x = 0; x < IMAGE_WIDTH; x++) {
                double pixel = image[x * IMAGE_WIDTH + y];
                String colour = ConsoleColours.WHITE_BOLD;
                if (pixel > 0.65) {
                    colour = ConsoleColours.BLACK_BOLD;
                }
                if (pixel > 0.75) {
                    colour = ConsoleColours.WHITE;
                }
                if (pixel > 0.95) {
                    colour = ConsoleColours.WHITE_BACKGROUND_BRIGHT;
                }
                System.out.print(colour + "██" + ConsoleColours.RESET);
            }
            System.out.println("");
        }
    }

    public void printOutput(int i) {
        System.out.println(mapping.get(Byte.toUnsignedInt(labels[i])));
    }

    public void printOutput(DoubleMatrix output) {
        int maxIndex = indexOf(output.toArray(), output.max());
        System.out.println(mapping.get(maxIndex));
    }
    
    private int indexOf(double[] array, double value) {
        for (int i = 0; i < array.length; i++) {
            double element = array[i];
            if (element == value) {
                return i;
            }
        }
        return -1;
    }
}
