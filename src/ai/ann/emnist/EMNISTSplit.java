/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.emnist;

/**
 *
 * @author samuel
 */
public enum EMNISTSplit {
    BYCLASS("-byclass"), BYMERGE("-bymerge"), BALANCED("-balanced"), LETTERS("-letters"), DIGITS("-digits"), MNIST("-mnist");
    String string;
    
    public enum EMNISTSet {
        TRAIN, TEST;
    }
    
    EMNISTSplit(String string) {
        this.string = "emnist" + string;
    }
}
