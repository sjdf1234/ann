/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.util;

/**
 *
 * @author samuel
 */
public class Stopwatch {
    
    private static long time = 0;
    
    public static void start() {
        time = System.nanoTime();
    }

    public static long stop() {
        return System.nanoTime() - time;
    }
}
