/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ocr;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author samuel
 */
public class Segmenter {
    // Image
    private int width;
    private int height;
    private int[][] image;
    
    // Current character
    private int minX, minY, maxX, maxY;
    
    public Segmenter(int[][] image) {
        this.width = image.length;
        this.height = image[0].length;
        this.image = image;
    }
    
    /**
     * Finds a character in the image, deletes it from the image and returns the
     * character
     *
     * @param image
     * @return
     */
    public int[] findNextCharacter() {
        int pixelX = -1;
        int pixelY = -1;
        
        // Scan the image
        horizontal:
        for (int x = 0; x < width; x++) {
            vertical:
            for (int y = 0; y < height; y++) {
                if (image[x][y] == 1) { // Find a pixel belonging to a character
                    pixelX = x;
                    pixelY = y;
                    break horizontal;
                }
            }
        }
        
        if (pixelX == -1) return null;

        // Reset character bounds
        minX = width;
        minY = height;
        maxX = 0;
        maxY = 0;
        
        // Find character bounds
        findCharacterBounds(pixelX, pixelY);
        
        // return new int[] {x, y, w, h};
        return new int[] { minX, minY, maxX - minX + 1, maxY - minY + 1 };

    }
    
    /**
     * Find character in the image, store it in characterBounds
     *
     * @param image
     * @param character
     * @param characterPixel
     */
    private void findCharacterBounds(int x, int y) {
        if (x >= width || x < 0) {
            return;
        }
        if (y >= height || y < 0) {
            return;
        }

        int target = 1;
        int replacement = 0;

        if (target == replacement) {
            return;
        }
        if (image[x][y] != target) {
            return;
        }

        image[x][y] = replacement;
        
        minX = Math.min(minX, x);
        minY = Math.min(minY, y);
        maxX = Math.max(maxX, x);
        maxY = Math.max(maxY, y);
        
        findCharacterBounds(x + 1, y);
        findCharacterBounds( x - 1, y);
        findCharacterBounds(x, y + 1);
        findCharacterBounds(x, y - 1);

    }
}
