/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.functions;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

/**
 *
 * @author samuel
 */
public class Sigmoid implements IActivationFunction {

    @Override
    public DoubleMatrix compute(DoubleMatrix X) {
        return MatrixFunctions.exp(X.mul(-1)).add(1).rdiv(1);
    }

    @Override
    public DoubleMatrix computeDerivative(DoubleMatrix fX) {
        return fX.mul(fX.rsub(1));
    }

    @Override
    public String toString() {
        return "1 / (e^-x + 1)";
    }

}
