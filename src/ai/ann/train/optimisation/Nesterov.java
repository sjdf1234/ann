/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.train.optimisation;

import org.jblas.DoubleMatrix;

/**
 *
 * @author samuel
 */
public class Nesterov extends Momentum {
    
    public Nesterov(double mu, double gamma) {
        super(mu, gamma);
    }
    
    @Override
    public DoubleMatrix[] calculateDeltaWeightMatrices(DoubleMatrix[] weightMatrices, DoubleMatrix[] gradientMatrices) {
        int size = weightMatrices.length;

        super.initOldDeltaWeightMatrices(size, weightMatrices);
        
        for (int i = 0; i < size; i++) {
            DoubleMatrix gammaOldDeltaMatrix = oldDeltaWeightMatrices[i].mul(-gamma);
            oldDeltaWeightMatrices[i] = gammaOldDeltaMatrix.addi(gradientMatrices[i].mul(-mu).mul(weightMatrices[i].add(gammaOldDeltaMatrix)));
        }
        return oldDeltaWeightMatrices;
    }
}
