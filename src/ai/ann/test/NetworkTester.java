/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.test;

import ai.ann.NeuralNetwork;
import ai.ann.util.IDataset;
import org.jblas.DoubleMatrix;

/**
 *
 * @author samuel
 */
public class NetworkTester {

    private NeuralNetwork network;

    private IDataset dataset;
    private int setSize;

    public NetworkTester() {
    }
    
    public NetworkTester(NeuralNetwork network, IDataset dataset) {
        setNetwork(network);
        setDataset(dataset);
    }
    
    /**
     * Sets the network to classificationTest
     *
     * @param network
     */
    public void setNetwork(NeuralNetwork network) {
        this.network = network;
    }

    /**
     * Sets the dataset to use in testing
     *
     * @param dataset
     */
    public void setDataset(IDataset dataset) {
        this.dataset = dataset;
        this.setSize = dataset.getSize();
    }

    public void classificationTest() {
        int total = dataset.getSize();
        int correct = 0;
        for (int i = 0; i < total; i++) {
            System.out.print(String.format("Testing:   Sample[%d/%d]\r", i + 1, setSize));

            DoubleMatrix observedOutput = network.feedForward(dataset.getInput(i));
            double max = observedOutput.max();
            for (int j = 0; j < observedOutput.length; j++) {
                if (observedOutput.get(j) == max) {
                    observedOutput.fill(0);
                    observedOutput.put(j, 1);
                    break;
                }
            }
            if (observedOutput.eq(dataset.getOutput(i)).min() == 1) {
                correct += 1;
            }
        }
        System.out.println();
        
        int incorrect = total - correct;
        double cPercentage = correct / (double) total * 100;
        double iPercentage = 100 - cPercentage;
        String result = String.format(
            "Correct:   %06.2f%% (%d)%n" + 
            "Incorrect: %06.2f%% (%d)%n" + 
            "Total:     %06.2f%% (%d)",
            cPercentage, correct,
            iPercentage, incorrect, 
            100.0, total); 
        System.out.println(result);
    }

    private void classify(DoubleMatrix output) {
        double max = output.max();
        for (int i = 0; i < output.length; i++) {
            if (output.get(i) == max) {
                output.fill(0);
                output.put(i, 1);
                return;
            }
        }
    }

}
