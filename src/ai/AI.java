/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai;

import ai.ann.NeuralNetwork;
import ai.ann.emnist.EMNISTDataset;
import ai.ann.emnist.EMNISTSet;
import ai.ann.emnist.EMNISTSplit;
import ai.ann.functions.SymmetricSigmoid;
import ai.ann.test.NetworkTester;
import ai.ann.test.TestResult;
import ai.ann.train.GDTrainer;
import ai.ann.train.optimisation.Default;
import ai.ocr.Segmenter;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import org.jblas.DoubleMatrix;

/**
 *
 * @author samuel
 */
public class AI {
    public static void main(String[] args) {
        // Run a test of the basic functions of this library
        // Correctly identified percentage should go from about 10% to about 90% after one epoch
        // The most accurate network I have ever trained after many training epochs is around 94% so
        // still not human-level but pretty accurate
        test();
        
        // Load in a BufferedImage from file
        
        // Convert it to a B+W DoubleMatrix
        
        // Feed a thresholded version of DoubleMatrix into the segmenter
        // to obtain character bounds 
        
        // Feed each character bound into a neural network to obtain character as char
        
    }
    
    public static void test() {
        // Create a neural network (784 input, 30 hidden, 10 output) 
        NeuralNetwork network = new NeuralNetwork(28 * 28);
        network.addLayer(30, new SymmetricSigmoid()); // Symmetric sigmoid is just hyperbolic tangent
        network.addLayer(10, new SymmetricSigmoid());
        network.print();
        
        // Load the datasets into RAM for quick access
        EMNISTDataset train = new EMNISTDataset("assets/gzip.zip", EMNISTSplit.MNIST, EMNISTSet.TRAIN);
        EMNISTDataset test = new EMNISTDataset("assets/gzip.zip", EMNISTSplit.MNIST, EMNISTSet.TEST);
        
        int inputIndex = 3453;
        train.printInput(inputIndex); // This is the input the network sees (28x28 greyscale image)
        train.printOutput(inputIndex); // This is the output the network should produce
        
        // Initalise a tester
        NetworkTester tester = new NetworkTester(network, test);
        tester.classificationTest(); // Run a test (outputs to console)
        
        // Train the network using 1 epoch of gradient descent with a mu value of 0.03
        GDTrainer trainer = new GDTrainer(network, train);
        trainer.setOptimisation(new Default(0.03f));
        trainer.trainStochasticGD(1);
        
        tester.classificationTest(); // Run a second test (to check if training succeeded)
    
        // Test save/load features work as intended
        String networkPath = "assets/network.json";
        network.save(networkPath);
        tester.setNetwork(NeuralNetwork.load(networkPath));
        tester.classificationTest(); // Run a third test (to check if same as second)
    }
    

}
