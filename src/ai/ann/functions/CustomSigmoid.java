/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.functions;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

/**
 *
 * @author samuel
 */
public class CustomSigmoid implements IActivationFunction {

    private final double scaleX, scaleY, c;
    
    public CustomSigmoid(double scaleX, double scaleY, double c) {
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.c = c;
    }
    
    @Override
    public DoubleMatrix compute(DoubleMatrix X) {
        return MatrixFunctions.exp(X.mul(scaleX).mul(-1)).add(1).rdiv(1).mul(scaleY).sub(c);
    }

    @Override
    public DoubleMatrix computeDerivative(DoubleMatrix fX) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
