/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.train.optimisation;

import org.jblas.DoubleMatrix;

/**
 *
 * @author samuel
 */
public class Momentum extends Default implements IOptimisation {

    public DoubleMatrix[] oldDeltaWeightMatrices;
    public final double gamma;

    public Momentum(double mu, double gamma) {
        super(mu);
        this.gamma = gamma;
    }
    
    @Override
    public DoubleMatrix[] calculateDeltaWeightMatrices(DoubleMatrix[] weightMatrices, DoubleMatrix[] gradientMatrices) {
        int size = weightMatrices.length;

        // Initialise old delta matrices if they don't already exist
        initOldDeltaWeightMatrices(size, weightMatrices);

        DoubleMatrix[] deltaWeightMatrices = new DoubleMatrix[size];
        DoubleMatrix[] superDeltaWeightMatrices = super.calculateDeltaWeightMatrices(weightMatrices, gradientMatrices);
        for (int i = 0; i < size; i++) {
            deltaWeightMatrices[i] = oldDeltaWeightMatrices[i].muli(-gamma).addi(superDeltaWeightMatrices[i]);
        }
        return deltaWeightMatrices;
    }

    protected void initOldDeltaWeightMatrices(int size, DoubleMatrix[] weightMatrices) {
        if (oldDeltaWeightMatrices == null) {
            oldDeltaWeightMatrices = new DoubleMatrix[size];
            for (int i = 0; i < size; i++) {
                oldDeltaWeightMatrices[i] = weightMatrices[i].dup().fill(0);
            }
        }
    }
}
