/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ocr;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author samuel
 */
public class Recogniser {

    private int width;
    private int height;
    private BufferedImage image;
    
    public Recogniser(String imagePath) {
        try (FileInputStream fis = new FileInputStream(imagePath)) {
            image = ImageIO.read(fis);
            width = image.getWidth();
            height = image.getHeight();
        } catch (IOException ex) {
            Logger.getLogger(Recogniser.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getString() {
        
        // Threshold the image to produce a binary version of it
        int[][] binary = new int[width][height];
        
        
        int[] charBounds;
        
        
        
        return null;
    }
}
