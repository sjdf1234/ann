/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import org.jblas.DoubleMatrix;
import ai.ann.functions.IActivationFunction;
import ai.ann.functions.IActivationFunctionAdapter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author samuel
 */
public class NeuralNetwork {

    private int inputSize;
    private List<DoubleMatrix> weightMatrices;
    private List<IActivationFunction> functions;
    private static String filepath;
    
    public NeuralNetwork(int inputSize) {
        if (inputSize < 1) {
            throw new IllegalArgumentException("Input size invalid");
        }
        
        this.inputSize = inputSize;
        this.weightMatrices = new ArrayList<>();
        this.functions = new ArrayList<>();
    }
    
    public void addLayer(int size, IActivationFunction function) {
        if (size < 1) {
            throw new IllegalArgumentException("Layer size invalid");
        }

        int inSize = weightMatrices.isEmpty() ? inputSize : weightMatrices.get(weightMatrices.size() - 1).rows;
        
        weightMatrices.add(DoubleMatrix.randn(size, inSize).mul(1 / 3f));
        functions.add(function);
    }

    public DoubleMatrix feedForward(DoubleMatrix input) {
        return feedForward(input, 0);
    }

    private DoubleMatrix feedForward(DoubleMatrix input, int i) {
        return i < getSize() ? feedForward(functions.get(i).compute(weightMatrices.get(i).mmul(input)), ++i) : input;
    }

    /**
     * Attempts to load a network from the specified file path
     * 
     * @param filepath
     * @return 
     */
    public static NeuralNetwork load(String filepath) {
        NeuralNetwork.filepath = filepath;
        try (BufferedReader br = new BufferedReader(new FileReader(filepath))) {
            String networkString = br.readLine();
            NeuralNetwork network = buildGson().fromJson(networkString, NeuralNetwork.class);
            System.out.println(String.format("Loaded: %s (%d bytes)", filepath, networkString.length()));
            return network;
        } catch (Exception e) {
            System.out.println(String.format("Loading network from file %s failed: %s", filepath, e.getMessage()));
            return null;
        }
    }

    /**
     * Attempts to save the network to the file it was loaded from
     * 
     */
    public void save() {
        if (filepath == null) {
            throw new RuntimeException("Network was not loaded; no path exists to save to");
        }
        save(filepath);
    }
    
    /**
     * Attempts to save the network from the specified file path
     * 
     * @param filepath 
     */
    public void save(String filepath) {
        File file = new File(filepath);
        try (FileOutputStream out = new FileOutputStream(file)) {
            String networkString = buildGson().toJson(this);
            out.write(networkString.getBytes());
            System.out.println(String.format("Saved: %s (%d bytes)", filepath, networkString.length()));
        } catch (Exception e) {
            System.out.println(String.format("Saving network to file %s failed: %s", filepath, e.getMessage()));
        }
    }

    /**
     * Helper method to ensure Gson serialises IActivationFunction instances properly
     * 
     * @return 
     */
    private static Gson buildGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(IActivationFunction.class, new IActivationFunctionAdapter());
        return builder.create();
    }

    /**
     * Returns the number of layers in this network
     * 
     * @return 
     */
    public int getSize() {
        return weightMatrices.size();
    }
    
    /**
     * Get the weights list as an array
     * 
     * @return 
     */
    public DoubleMatrix[] getWeightMatrices() {
        return weightMatrices.toArray(new DoubleMatrix[weightMatrices.size()]);
    }
    
    private void setWeightMatrices(ArrayList<DoubleMatrix> weightMatrices) {
        this.weightMatrices = weightMatrices;
    }
    
    /**
     * Get the activation functions list as an array
     * 
     * @return 
     */
    public IActivationFunction[] getFunctions() {
        return functions.toArray(new IActivationFunction[functions.size()]);
    }

    private void setFunctions(ArrayList<IActivationFunction> functions) {
        this.functions = functions;
    }
    
    /**
     * Should be called after load(String filepath)
     */
    public void assertNonCorrupt() {
        String message = null;
        for (int i = 0; i < getSize(); i++) {
            DoubleMatrix weightMatrix = weightMatrices.get(i);
            if (weightMatrix.rows * weightMatrix.columns != weightMatrix.length || weightMatrix.length != weightMatrix.data.length) {
                message = String.format("Corrupt matrix on layer %d", i);
                break;
            }
        }

        if (message != null) {
            throw new RuntimeException(message);
        }
    }

    @Override
    public String toString() {
        int size = getSize();
        int i = 0;
        
        String string = String.format("%d", weightMatrices.get(i).columns);
        for (; i < size; i++) {
            string += String.format(" > %d[%s]", weightMatrices.get(i).rows, functions.get(i).getClass().getSimpleName());
        }  
        return string;
    }

    public void print() {
        System.out.println(toString());
    }
}
