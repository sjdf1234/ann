package ai.ann.util;

/**
 * A fairly straight-forward matrices implementation, capable of basic operations
 * 
 * 
 * @author samuel
 */
import ai.ann.util.ConsoleColours;
import com.google.gson.Gson;
import java.util.Arrays;
import java.util.Random;
public class MatrixOld {

    private final int rows;
    private final int cols;
    private double[] elements;
    
    /*
    public MatrixOld(String filename) {
       
    }
    */
    public String save(String filename) {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
    
    public MatrixOld(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        this.elements = new double[rows * cols];
    }
    
    public MatrixOld(int size) {
        this(size, size);
    }
    
    /**
     * Zeroes a matrix
     * 
     * @return 
     */
    public MatrixOld zero() {
        for (int i = 0; i < elements.length; i++) 
            elements[i] = 0;
        return this;
    }

    /**
     * Creates an identity matrix, if possible (rows has to be equal to cols)
     * 
     * @return 
     */
    public MatrixOld identity() {
        if (rows != cols)
            throw new IllegalArgumentException("Column count must = row count to create identity matrix");
            
        for (int i = 0; i < rows; i++) {
            this.setElement(i, i, 1);
        }
        return this;
    }
    
    /**
     * Randomises a matrix
     * Uses symmetric random initialisation
     * 
     * @return 
     */
    public MatrixOld randomise() {
        for (int i = 0; i < elements.length; i++) {
            double value = new Random().nextDouble();
            if (i % 2 == 0) {
                value *= -1;
            }
            //value = new Random().nextGaussian();
            elements[i] = value;
            
        }
        return this;
    }

    public MatrixOld add(MatrixOld matrix) {
        if (!consistentDimensions(matrix))
            throw new IllegalArgumentException("Dimensions inconsistent");
                
        double[] addends = matrix.getElements();
        for (int i = 0; i < elements.length; i++) {
            elements[i] += addends[i];
        }
        return this;
    }

    public MatrixOld sub(MatrixOld matrix) {
        if (!consistentDimensions(matrix))
            throw new IllegalArgumentException("Dimensions inconsistent");
        
        double[] addends = matrix.getElements();
        for (int i = 0; i < elements.length; i++) {
            elements[i] -= addends[i];
        }
        return this;
    }

    public MatrixOld mul(double scalar) {
        for (int i = 0; i < elements.length; i++) {
            elements[i] *= scalar;
        }
        return this;
    }

    public MatrixOld mul(MatrixOld matrix) {
        if (cols != matrix.getRows()) {
            throw new IllegalArgumentException("Column count of multiplicand A must = row count of multiplicand B");
        }
        
        MatrixOld product = new MatrixOld(rows, matrix.getCols());
        for (int i = 0; i < product.getRows(); i++) {
            for (int j = 0; j < product.getCols(); j++) {
                double element = 0;
                for (int k = 0; k < cols; k++) {
                    element += getElement(i, k) * matrix.getElement(k, j);
                }
                product.setElement(i, j, element);
            }
        }
        return product;
    }

    /**
     * Transposes a matrix
     * 
     * @return 
     */
    public MatrixOld T() {
        MatrixOld transpose = new MatrixOld(cols, rows);
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                transpose.setElement(j, i, getElement(i, j));
            }
        }
        return transpose;
    }

    /**
     * Copies a matrix
     * 
     * @return 
     */
    public MatrixOld cpy() {
        MatrixOld clone = new MatrixOld(rows, cols);
        clone.setElements(elements);
        return clone;
    }

    public int getRows() {
        return rows; 
    }

    public int getCols() {
        return cols;
    }

    public int getElementCount() {
        return rows * cols;
    }
    
    public double getElement(int row, int col) {
        if (row < 0 || row >= rows || col < 0 || col >= cols) {
            throw new IndexOutOfBoundsException("No such indicies");
        }
        return elements[row * cols + col];
    }

    public MatrixOld setElement(int row, int col, double element) {
        if (row < 0 || row >= rows || col < 0 || col >= cols) {
            throw new IndexOutOfBoundsException("No such indicies");
        }
        elements[row * cols + col] = element;
        return this;
    }
    
    public MatrixOld addToElement(int row, int col, double addend) {
        setElement(row, col, getElement(row, col) + addend);
        return this;
    }
    
    public double[] getElements() {
        return elements;
    }
    
    public void setElements(double[] elements) {
        if (this.elements.length == elements.length) 
            this.elements = Arrays.copyOf(elements, elements.length);
        else
            throw new IllegalArgumentException("Incorrect element array size");
    }

    
    public MatrixOld print(int digitCount) {
        int maxLength = digitCount + 2; // +2 accounts for decimal point and sign characters

        String separator = "";
        while (separator.length() < maxLength * cols + cols - 1) {
            separator += "~";
        }
        //System.out.println(separator);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                double element = getElement(i, j);

                String formattedElement = Double.toString(element);
                // Pad with leading space if positive
                if (element >= 0) {
                    formattedElement = "+" + formattedElement;
                }
                // Pad with trailing zeros if length is smaller than maximum
                while (formattedElement.length() < maxLength) {
                    formattedElement += "0";
                }
                // Restrict the displayed length to the specified maximum
                formattedElement = formattedElement.substring(0, maxLength);

                // Apply colours
                String colour = ConsoleColours.BLACK_BOLD; // -0.1 < element < 0.1
                if (element <= -0.5) {
                    colour = ConsoleColours.RED_BOLD;
                } else if (element > -0.5 && element <= -0.1) {
                    colour = ConsoleColours.RED;
                } else if (element >= 0.1 && element < 0.5) {
                    colour = ConsoleColours.GREEN;
                } else if (element >= 0.5) {
                    colour = ConsoleColours.GREEN_BOLD;
                } 
                formattedElement = colour + formattedElement + ConsoleColours.RESET;
    
                System.out.print(formattedElement + " ");
            }
            System.out.println();
        }
        //System.out.println(separator);
        return this;
    }

    public MatrixOld print() {
        return print(2);
    }
    
    /**
     * Checks if the add/sub operation is defined for two given matrices
     * 
     * @param m2 the matrix to be checked against
     * @return 
     */
    public boolean consistentDimensions(MatrixOld m2) {
        boolean rowsConsistent = getRows() == m2.getRows();
        boolean colsConsistent = getCols() == m2.getCols();
        if (rowsConsistent && colsConsistent) 
            return true;
        
        return false;
    }
    
    /**
     * Tests for square matrix
     * 
     * @return 
     */
    public boolean isSquare() {
        return rows == cols;
    }
    
    /**
     * Tests for row matrix
     * 
     * @return 
     */
    public boolean isRow() {
        return rows == 1;
    }
    
    /**
     * Tests for column matrix
     * 
     * @return 
     */
    public boolean isCol() {
        return cols == 1;
    }
}
