/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.train.optimisation;

import org.jblas.DoubleMatrix;

/**
 *
 * @author samuel
 */
public interface IOptimisation {
    
    public DoubleMatrix[] calculateDeltaWeightMatrices(DoubleMatrix[] weightMatrices, DoubleMatrix[] gradientMatrices);
    
}
