/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.functions;

import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;

/**
 *
 * @author samuel
 */
public class SymmetricStep extends Step {

    @Override
    public DoubleMatrix compute(DoubleMatrix X) {
        return MatrixFunctions.ceil(X).mul(2).sub(1);
    }

    @Override
    public String toString() {
        return "2 * ceil(x) - 1";
    }

}
