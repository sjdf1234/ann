/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.train;

import ai.ann.util.ActivationFunction_ScalarImpl;
//import ai.ann.util.DataOld;
import ai.ann.util.MatrixOld;
import ai.ann.NeuralLayer_ScalarImpl;
import ai.ann.NeuralNetwork_ScalarImpl;
import ai.ann.util.Util;

/**
 * A basic SGD network trainer
 *
 * @author samuel
 */
public class NetworkTrainer_ScalarImpl {
//    private final NeuralNetwork_ScalarImpl network;
//    private int maxEpochs = 5;
//    private double learningRate = 0.0008;    
//    private int batchSize = 1;
//    private OptimisationType optType = OptimisationType.MOMENTUM;
//    
//    public NetworkTrainer_ScalarImpl(NeuralNetwork_ScalarImpl network) {
//        this.network = network;
//    }
//    
//    public void train(DataOld data) {
//        for (int epoch = 0; epoch < this.maxEpochs; epoch++) {
//            data.randomise();
//            
//            int batchCount = data.getSize() / batchSize;
//            for (int batchIndex = 0; batchIndex < batchCount; batchIndex++) {
//                System.out.print("Epoch " + (epoch + 1) + " of " + this.maxEpochs);
//                System.out.print(", Batch " + (batchIndex + 1) + " of " + batchCount + "\r");
//                DataOld batch = data.createBatch(batchSize, batchIndex);
//                backPropagateError(batch);
//                network.updateWeights(optType, batchSize, learningRate);
//            }
//            //backPropagateError(data.getInputs(), data.getExpectedOutputs());
//            System.out.println();
//        }
//    }
//
//    /**
//     * Calculates the error values for each neuron in each layer of a neural
//     * network using the back propagation algorithm and Stochastic Gradient
//     * Descent
//     *
//     * @param inputs
//     * @param expectedOutputs
//     */
//    private void backPropagateError(DataOld batch) {
//        double[][] inputs = batch.getInputs();
//        double[][] expectedOutputs = batch.getExpectedOutputs();
//        NeuralLayer_ScalarImpl[] layers = network.getNeuralLayers();
//        
//        for (int i = 0; i < inputs.length; i++) {
//            double[] input = inputs[i];
//            double[] expectedOutput = expectedOutputs[i];
//            double[] observedOutput = network.feedForward(input).getElements();
//            double error = Util.SE(observedOutput, expectedOutput);
//            
//            for (int j = layers.length - 1; j >= 0; j--) {
//                // Current layer
//                NeuralLayer_ScalarImpl layer = layers[j];
//                MatrixOld activations = layer.getActivations();
//                ActivationFunction_ScalarImpl af = layer.getActivationFunction();
//                MatrixOld errors = layer.getErrors();
//                
//                /**
//                 * Code not very DRY past this point; needs some work but is 
//                 * still functional at least. The reason for this is that the
//                 * last (output) layer has to be handled differently than the
//                 * other layers as it does not have a "next" layer to generate
//                 * error values from, instead its error values are calculated
//                 * directly from its output and the expected output of the
//                 * training/validation/test set.
//                 */
//                if (j == layers.length - 1) {
//                    for (int k = 0; k < layer.getNeuronCount(); k++) {
//                        double dE_da = activations.getElement(k, 0) - expectedOutput[k];
//                        double da_dz = af.invokeDerivative(activations.getElement(k, 0));
//                        double dE_dz = dE_da * da_dz;
//                        errors.setElement(k, 0, dE_dz);
//                    }
//                } else {
//                    // Next layer (the one that this would feed into)
//                    NeuralLayer_ScalarImpl nextLayer = layers[j + 1];
//                    MatrixOld nextWeights = nextLayer.getWeights();
//                    MatrixOld nextErrors = nextLayer.getErrors();
//                    
//                    for (int k = 0; k < layer.getNeuronCount(); k++) {
//                        double dE_da = 0;
//                        for (int l = 0; l < nextLayer.getNeuronCount(); l++) {
//                            dE_da += nextWeights.getElement(l, k) * nextErrors.getElement(l, 0);
//                        }
//                        double da_dz = af.invokeDerivative(activations.getElement(k, 0));
//                        double dE_dz = dE_da * da_dz;
//                        errors.setElement(k, 0, dE_dz);
//                    }
//                }
//                
//            }
//            //network.updateWeights(learningRate);
//            network.updateGradients();
//        }
//    }
}
