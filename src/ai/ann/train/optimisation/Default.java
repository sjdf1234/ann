/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.train.optimisation;

import ai.ann.train.optimisation.IOptimisation;
import org.jblas.DoubleMatrix;

/**
 *
 * @author samuel
 */
public class Default implements IOptimisation {
        public final double mu;
        
        public Default(double mu) {
            this.mu = mu;
        }

        @Override
        public DoubleMatrix[] calculateDeltaWeightMatrices(DoubleMatrix[] weightMatrices, DoubleMatrix[] gradientMatrices) {
            int size = weightMatrices.length;
            DoubleMatrix[] deltaWeightMatrices = new DoubleMatrix[size];
            for (int i = 0; i < size; i++) {
                deltaWeightMatrices[i] = gradientMatrices[i].mul(-mu);
            }
            return deltaWeightMatrices;
        }
    }
