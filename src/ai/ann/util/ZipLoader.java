/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author samuel
 */
public class ZipLoader {

    private ZipFile zip;

    public ZipLoader(String path) {
        try {
            zip = new ZipFile(path);
        } catch (IOException ex) {
            Logger.getLogger(ZipLoader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public byte[] loadGZIPEntry(String path) {
        ZipEntry entry = zip.getEntry(path);
        
        try (GZIPInputStream gzip = new GZIPInputStream(zip.getInputStream(entry))) {
            byte[] bytes = IOUtils.toByteArray(gzip);
            System.out.println(String.format("Loaded: %s (%d compressed bytes)", entry.getName(), entry.getSize()));
            return bytes;
        } catch (Exception e) {
            String msg = String.format("Loading failed: %s (%d compressed bytes)", entry.getName(), entry.getSize());
            Logger.getLogger(ZipLoader.class.getName()).log(Level.SEVERE, msg, e);
            return null;
        }
    }

    public String[] loadTextEntry(String path) {
        ZipEntry entry = zip.getEntry(path);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(zip.getInputStream(entry)))) {
            ArrayList<String> lines = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
            
            String[] linesArray = new String[lines.size()];
            for (int i = 0; i < linesArray.length; i++) {
                linesArray[i] = lines.get(i);
            }
            
            System.out.println(String.format("Loaded: %s (%d bytes)", entry.getName(), entry.getSize()));
            return linesArray;
        } catch (Exception e) {
            String msg = String.format("Loading failed: %s (%d bytes)", entry.getName(), entry.getSize());
            Logger.getLogger(ZipLoader.class.getName()).log(Level.SEVERE, msg, e);
            return null;
        }
    }
}
