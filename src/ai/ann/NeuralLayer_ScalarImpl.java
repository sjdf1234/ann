/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann;

import ai.ann.util.MatrixOld;
import ai.ann.util.ActivationFunction_ScalarImpl;
import ai.ann.train.optimisation.IOptimisation;

/**
 *
 * @author samuel
 */
public class NeuralLayer_ScalarImpl {
    private final int neuronCount;
    private final int inputCount;

    private transient MatrixOld inputs, sums, activations; // Feed Foward
    private MatrixOld weights;
    private transient MatrixOld errors, gradients; // Back Propagate
    private ActivationFunction_ScalarImpl af;
    
    /**
     * Creates a new neural layer with randomised weights
     * 
     * @param neuronCount number of neurons in layer
     * @param inputCount number of inputs per neuron
     */
    public NeuralLayer_ScalarImpl(int neuronCount, int inputCount) {
        //inputCount += 1; // To account for bias term
        this.neuronCount = neuronCount;
        this.inputCount = inputCount;
        
        weights = new MatrixOld(neuronCount, inputCount).randomise(); // New random matrix
        
        init();
        
        if (neuronCount < 1) {
            throw new IllegalArgumentException("neuronCount must be > 0");
        }
        
        if (inputCount < 1) {
            throw new IllegalArgumentException("inputCount must be > 0");
        }
        
        af = ActivationFunction_ScalarImpl.SIGMOID; // Default activation function
    }
    
    /**
     * Init/Reset function for all layer values other than the ones
     * saved in a JSON format. Called after loading a net from a file.
     * 
     * 
     */
    public void init() {
        inputs = new MatrixOld(inputCount, 1);
        
        sums = new MatrixOld(neuronCount, 1);
        activations = sums.cpy();
        errors = sums.cpy();
        
        gradients = new MatrixOld(neuronCount, inputCount);
    }
    
    /**
     * Feed inputs through this layer, storing intermediates for later use by
     * back propagation algorithm 
     * 
     * @param inputs An input matrix of dimensions inputCount by 1
     * @return 
     */
    public MatrixOld feedForward(MatrixOld inputs) {
        this.inputs = inputs;
        sums = weights.cpy().mul(inputs); // Calculate weighted sum of inputs
        
        for (int i = 0; i < sums.getRows(); i++) {
                    double sum = sums.getElement(i, 0);
                    double activation = af.invoke(sum);
                    activations.setElement(i, 0, activation);
        }
        return activations;
    }
    /**
     * Same as feedFoward(MatrixOld inputs) except that it accepts an array as input vector
     * 
     * @param inputArray
     * @return 
     */
    public MatrixOld feedForward(double[] inputArray) {
        MatrixOld inputs = new MatrixOld(inputArray.length, 1);
        inputs.setElements(inputArray);
        return feedForward(inputs);
    }
    
    /**
     * Keeps a running total of weight gradient values, for averaging and updating
     * the weights later
     * 
     */
    public void updateGradients() {
        for (int i = 0; i < neuronCount; i++) {
            for (int j = 0; j < inputCount; j++) { 
                // G: i by j
                // E: i by 1
                // I: j by 1
                // G = EI^T
                
                // Derivative of error function w.r.t this weight (the "gradient" of E at w)
                double dE_dw = errors.getElement(i, 0) * inputs.getElement(j, 0);
                gradients.addToElement(i, j, dE_dw);
            }
        }
    }
    
    /**
     * Averages the gradient total based on sample size, updates the weights based
     * on average gradient and learning rate
     * 
     * @param batchSize 
     * @param learningRate 
     */
    public void updateWeights(IOptimisation optimisationType, int batchSize, double learningRate) {
        // Calculate average gradients and scale
        gradients.mul(1.0 / (double) batchSize).mul(learningRate);
        // Subtract the scaled gradients from the weights
        weights.sub(gradients);
        // Zero the total
        gradients.zero();
    }
    
    /**
     * Updates the weights in order to minimise error for this layer
     * Used in training
     * 
     * @param learningRate 
     */
    /*
    public void updateWeights(double learningRate) {
        for (int i = 0; i < neuronCount; i++) {
            for (int j = 0; j < inputCount; j++) { 
                // Derivative of error function w.r.t this weight (the "gradient" of E at w)
                double dE_dw = errors.getElement(i, 0) * inputs.getElement(j, 0);
                // To minimise error we need to subtract this gradient, multiplied by a constant
                double dw = -1 * learningRate * dE_dw;
                gradients.setElement(i, j, dw);
            }
        }
        // Change the weights according to calculated gradients
        weights.add(gradients);
    }
    */
    
    public int getNeuronCount() {
        return neuronCount;
    }
    
    public int getInputCount() {
        return inputCount;
    }
    
    public MatrixOld getWeights() {
        return weights;
    }
    
    public MatrixOld getSums() {
        return sums;
    }
    
    public MatrixOld getActivations() {
        return activations;
    }
    
    public MatrixOld getErrors() {
        return errors;
    }
    
    public ActivationFunction_ScalarImpl getActivationFunction() {
        return af;
    }
    
    public void setActivationFunction(ActivationFunction_ScalarImpl activationFunction) {
        this.af = activationFunction;
    }
    
    /**
     * Prints the current state of the neural layer
     * 
     */
    public void print() {
        System.out.println("NEURAL LAYER");
        System.out.println("Hyperparameters: ");
        System.out.println("\tNeuron count: " + neuronCount);
        System.out.println("\tInputs per neuron: " + inputCount);
        System.out.println("\tActivation Function: " + af.name());
        System.out.println();
        
        System.out.println("Current state information: ");
        System.out.println("\tWeights, W = "); weights.print();
        System.out.println("\tInputs, I = "); inputs.print();
        System.out.println("\tWeight input product, WI = "); sums.print();
        System.out.println("\tActivated weight input product, act(WI) ="); activations.print();
        System.out.println();
    }
}
