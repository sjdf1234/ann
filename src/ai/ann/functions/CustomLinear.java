/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.functions;

import org.jblas.DoubleMatrix;

/**
 *
 * @author samuel
 */
public class CustomLinear implements IActivationFunction {

    private final double m;

    public CustomLinear(double m) {
        this.m = m;
    }
    
    @Override
    public DoubleMatrix compute(DoubleMatrix X) {
        return X.mul(m);
    }

    @Override
    public DoubleMatrix computeDerivative(DoubleMatrix fX) {
        return fX.dup().fill(m);
    }

    @Override
    public String toString() {
        return String.format("%.0f * x", m);
    }

}
