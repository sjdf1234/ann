/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.util;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Random;

/**
 * Matrix functions for 2D Double arrays designed specifically for use in
 Neural Networks. Functions do not error-check for speed. 
 * 
 * All unary operations (functions that operate on one matrix only) will
 * accept any matrix A such that isValid(A) == true
 * 
 * All binary operations (functions that operate one two matrices only) will
 * accept any matrices A and B such that is[Method]Valid(A, B) == true
 * 
 * @author samuel
 */
public class Matrix {

    public static double[][] newZeroMatrix(int m, int n) {
        double[][] Z = new double[m][n];
        clear(Z);
        return Z;
    }
    
    public static void clear(double[][] A) {
        int m = A.length;
        int n = A[0].length;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                A[i][j] = 0;
            }
        }
    }
    
    public static double[][] newRowMatrix(double... v) {
        return new double[][] { v }; 
    }
    
    public static boolean isRowMatrix(double[][] A) {
        return A.length == 1;
    }
    
    public static double[][] newColumnMatrix(double... v) {
        return transpose(newRowMatrix(v));
    }
    
    public static boolean isColumnMatrix(double[][] A) {
        return isValid(A) && A[0].length == 1;
    }
    
    public static double[] toVector(double[][] A) {
        if (isRowMatrix(A)) {
            return A[0];
        } else if (isColumnMatrix(A)) {
            return transpose(A)[0];
        } else {
            throw new IllegalArgumentException("Matrix is not a vector");
        }
    }
    
    public static double[][] newRandomMatrix(int m, int n) {
        double[][] R = new double[m][n];
        randomise(R);
        return R;
    }
    
    public static void randomise(double[][] A) {
        int m = A.length;
        int n = A[0].length;

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                A[i][j] = new Random().nextDouble() * Math.pow(-1, i + j);
            }
        }
    }

    public static boolean isValid(double[][] A) {
        for (double[] row : A) {
            if (row.length != A[0].length) {
                return false;
            }
        }
        return true;
    }
    
    public static void validate(double[][] A) {
        if (!isValid(A)) throw new IllegalArgumentException("Double array not a valid matrix");
    }
    
    public static double[][] cpy(double[][] A) {
        int m = A.length;
        int n = A[0].length;

        double[][] C = new double[m][n];
        for (int i = 0; i < m; i++) {
            C[i] = A[i].clone();
        }
        return C;
    }

    public static double[][] transpose(double[][] A) {
        int m = A.length;
        int n = A[0].length;

        double[][] T = new double[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                T[i][j] = A[j][i];
            }
        }
        return T;
    }

    public static double[][] add(double[][] A, double[][] B, boolean inPlace) {
        int m = A.length;
        int n = A[0].length;

        double[][] C = inPlace ? A : cpy(A);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                C[i][j] += B[i][j];
            }
        }
        return C;
    }

    public static double[][] sub(double[][] A, double[][] B, boolean inPlace) {
        int m = A.length;
        int n = A[0].length;

        double[][] C = inPlace ? A : cpy(A);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                C[i][j] -= B[i][j];
            }
        }
        return C;
    }
    
    public static boolean isAddSubValid(double[][] A, double[][] B) {
        return isValid(A) && isValid(B) && A.length == B.length && A[0].length == B[0].length;
    }
    
    public static void validateAddSub(double[][] A, double[][] B) {
        if (!isAddSubValid(A, B)) throw new IllegalArgumentException("Add/Sub not valid");
    }

    public static double[][] mul(double[][] A, double a, boolean inPlace) {
        int m = A.length;
        int n = A[0].length;

        double[][] B = inPlace ? A : cpy(A);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                B[i][j] *= a;
            }
        }
        return B;
    }
    
    public static double[][] mul(double[][] A, double[][] B) {
        int m = A.length;
        int n = A[0].length;
        int o = B[0].length;

        double[][] C = newZeroMatrix(m, o);
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < o; j++) {
                for (int k = 0; k < n; k++) {
                    C[i][j] += A[i][k] * B[k][j];
                }
            }
        }
        return C;
    }
    
    public static boolean isMulValid(double[][] A, double[][] B) {
        return isValid(A) && isValid(B) && A[0].length == B.length;
    }
    
    public static void validateMul(double[][] A, double[][] B) {
        if (!isMulValid(A, B)) throw new IllegalArgumentException("Mul not valid");
    }
    
    public static void prettyPrint(double[][] A) {
        int m = A.length;
        int n = A[0].length;
        String string = "";
        String BRACKET= "|", ELEMENT_SEPARATOR = " ";
        
        DecimalFormat formatter = new DecimalFormat();
        formatter.setPositivePrefix("+");
        formatter.setGroupingUsed(false);
        formatter.setMinimumIntegerDigits(1);
        formatter.setMaximumIntegerDigits(1);
        formatter.setDecimalSeparatorAlwaysShown(true);
        formatter.setMinimumFractionDigits(20);
        formatter.setMaximumFractionDigits(20);
        for (int i = 0; i < m; i++) {
            string += BRACKET;
            for (int j = 0; j < n; j++) {
                string += formatter.format(A[i][j]);
                string += (j < n - 1) ? ELEMENT_SEPARATOR : "";
            }
            string += BRACKET + "\n";
        }

        System.out.print(string);
    }
    
    public static void quickPrint(double[][] A) {
        String string = Arrays.deepToString(A);
        string = string.substring(1, string.length() - 1);
        string = string.replaceAll("\\], \\[", "\\]\n\\[");
        System.out.println(string);
    }
}
