/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.emnist;

/**
 *
 * @author samuel
 */
public enum EMNISTSet {
    TRAIN("-train"), TEST("-test");
    String string;

    EMNISTSet(String string) {
        this.string = string;
    }
}
