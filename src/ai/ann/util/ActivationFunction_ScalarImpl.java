/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.util;

/**
 *
 * @author samuel
 */
public enum ActivationFunction_ScalarImpl {

    STEP(FunctionType.STEP),
    LINEAR(FunctionType.LINEAR),
    SIGMOID(FunctionType.SIGMOID),
    HYPERTAN(FunctionType.HYPERTAN);

    private enum FunctionType {
        STEP, LINEAR, SIGMOID, HYPERTAN;
    }

    FunctionType type;

    private ActivationFunction_ScalarImpl(FunctionType type) {
        this.type = type;
    }

    /**
     * Invoke this activation function
     *
     * @param x
     * @return
     */
    public double invoke(double x) {
        switch (type) {
            case STEP:
                if (x < 0) {
                    return 0;
                }
                return 1;
            case LINEAR:
                return x;
            case SIGMOID:
                return (1.0 / (1.0 + Math.exp(-x)));
            case HYPERTAN:
                return Math.tanh(x);
            default:
                throw new IllegalArgumentException("Function is not defined");
        }
    }

    /**
     * Invoke the derivative of this activation function
     *
     * IMPORTANT: these function derivatives ( f'(x) ) are presented in terms of their
     * respective functions ( f(x) ), not in terms of the original function parameter ( x ).
     * Also, for some functions derivatives do not exist.
     *
     * E.g. f(x) = 1 / (1 + e^-x) => f'(x) = f(x) * (1 - f(x))
     *
     * @param fx
     * @return
     */
    public double invokeDerivative(double fx) {
        switch (type) {
            case STEP:
                if (fx == 0) {
                    throw new IllegalArgumentException("");
                }
                return 0;
            case LINEAR:
                return 1;
            case SIGMOID:
                return fx * (1 - fx);
            case HYPERTAN:
                return 1 - Math.pow(fx, 2);
            default:
                throw new IllegalArgumentException("Function derivative is not defined");
        }
    }

}
