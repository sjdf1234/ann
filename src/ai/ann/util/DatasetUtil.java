/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.util;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author samuel
 */
public class DatasetUtil {
    
    public static int[] getIndices(IDataset dataset) {
        int size = dataset.getSize();
        int[] indices = new int[size];
        for (int i = 0; i < size; i++) {
            indices[i] = i;
        }
        return indices;
    }
    
    public static void shuffleIndices(int[] indices) {
        // Richard Durstenfeld's version of the Fisher–Yates shuffle
        Random random = new Random();
        for (int i = indices.length - 1; i > 0; i--) {
            int index = random.nextInt(i + 1);
            // Simple swap
            int a = indices[index];
            indices[index] = indices[i];
            indices[i] = a;
        }
    }
    
    public static int[][] getBatches(int[] indices, int batchSize) {
        int batchCount = indices.length / batchSize;
        int[][] batches = new int[batchCount][batchSize];
        for (int i = 0; i < batchCount; i++) {
            int begin = i * batchSize;
            int end = begin + batchSize;
            batches[i] = Arrays.copyOfRange(indices, begin, end);
        }
        return batches;
    }
}
