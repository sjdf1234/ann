/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.test;

/**
 *
 * @author samuel
 */
public class TestResult {
    public final int total;
    public final int correct;
    public final int incorrect;
    public final double cPercentage;
    public final double iPercentage;
    
    public TestResult(int total, int correct) {
        this.total = total;
        this.correct = correct;
        incorrect = total - correct;
        cPercentage = (correct / (double) total) * 100;
        iPercentage = 100 - cPercentage;
    }
    
    @Override
    public String toString() {
        return String.format(
            "Correct:   %06.2f%% (%d)%n" + 
            "Incorrect: %06.2f%% (%d)%n" + 
            "Total:     %06.2f%% (%d)",
            cPercentage, correct,
            iPercentage, incorrect, 
            100.0, total);    
    }
    
    public void print() {
        System.out.println(this);
    }
}

