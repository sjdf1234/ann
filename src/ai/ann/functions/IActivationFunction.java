/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.functions;

import com.google.gson.InstanceCreator;
import com.google.gson.annotations.SerializedName;
import java.lang.reflect.Type;
import org.jblas.DoubleMatrix;
import org.jblas.MatrixFunctions;
/**
 *
 * @author samuel
 */


public interface IActivationFunction {
    
    /**
     * Applies the function f(X) element-wise to X
     * 
     * @param X
     * @return 
     */
    public DoubleMatrix compute(DoubleMatrix X);
    
    /**
     * Applies the first derivative of the function f'(X) element-wise to f(X)
     * Note that f'(X) is in terms of f(X) rather than X for efficiency reasons
     * To apply first derivative in terms of X, use f'(f(X)) 
     * 
     * @param fX 
     * @return  
     */
    public DoubleMatrix computeDerivative(DoubleMatrix fX);
}
