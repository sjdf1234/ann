/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.util;

import org.jblas.DoubleMatrix;

/**
 *
 * @author samuel
 */
public interface IDataset {
    
    public int getSize();
    
    public DoubleMatrix getInput(int i);
    public DoubleMatrix getOutput(int i);
}
