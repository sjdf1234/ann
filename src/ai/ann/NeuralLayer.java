/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann;

import ai.ann.util.ActivationFunction_ScalarImpl;
import ai.ann.util.Matrix;
import org.jblas.DoubleMatrix;
import ai.ann.functions.IActivationFunction;
import ai.ann.functions.IActivationFunction;

/**
 *
 * @author samuel
 */
public class NeuralLayer {
    
    private final IActivationFunction f;
    private final DoubleMatrix W;
    
    /**
     * Create a new neural layer representation
     * 
     * @param inputSize number of input neurons for this layer
     * @param outputSize number of output neurons for this layer
     * @param f the activation for this layer
     */
    public NeuralLayer(int inputSize, int outputSize, IActivationFunction f) {
        this.f = f;
        W = DoubleMatrix.randn(outputSize, inputSize);
        W.muli(1/3.0);
    }

    public DoubleMatrix feedForward(DoubleMatrix X) {
        return f.compute(W.mmul(X)); 
    }
    
    @Override
    public String toString() {
        return String.format("[%d, %d, %s]", getInputSize(), getOutputSize(), f);
    }
    
    public int getInputSize() {
        return W.getColumns();
    }
    
    public int getOutputSize() {
        return W.getRows();
    }
    
    public IActivationFunction getF() {
        return f;
    }
    
    public DoubleMatrix getW() {
        return W;
    }

}