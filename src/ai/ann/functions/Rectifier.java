/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.functions;

import org.jblas.DoubleMatrix;

/**
 *
 * @author samuel
 */
public class Rectifier implements IActivationFunction {

    @Override
    public DoubleMatrix compute(DoubleMatrix X) {
        return X.max(0);
    }

    @Override
    public DoubleMatrix computeDerivative(DoubleMatrix fX) {
        return fX.gt(0);
    }
    
    @Override
    public String toString() {
        return "max(x, 0)";
    }
    
}
