/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ai.ann.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author samuel
 */
public class Util {
    
    /**
     * Calculates the Squared Error (SE) between two arrays
     * 
     * @param observedOutput
     * @param expectedOutput
     * @return 
     */
    public static double SE(double[] observedOutput, double[] expectedOutput) {
        if (observedOutput.length != expectedOutput.length) {
            throw new IllegalArgumentException("The lengths of the actual and expected value arrays must be equal");
        }
        
        double sum = 0;
        for (int i = 0; i < expectedOutput.length; i++) {
            sum += Math.pow(observedOutput[i] - expectedOutput[i], 2);
        }
        
        return sum / 2;
    }
    
    /**
     * Find the index of the maximum valued element of an array
     * 
     * @param array
     * @return 
     */
    public static int maxDoubleArray(double[] array) {
        double max = array[0]; 
        for (int i = 1; i < array.length; i++) {
            double element = array[i];
            if (element > max) {
                max = element;
            }
        }
        for (int i = 0; i < array.length; i++) {
            double element = array[i];
            if (element == max) {
                return i;
            }
        }
        return -1;
    }
}
